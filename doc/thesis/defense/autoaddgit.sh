infile=$1
allimages=`cat $infile | grep -v "^%" | grep inclu | sed 's#.*\(images/.*\)\}.*$#\1#' | sed "s/[\{\}]//g"`

for i in $allimages;
do
    echo "adding $i to git"
    git add -f $i
done
